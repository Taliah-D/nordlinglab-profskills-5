* This diary file is written by YuChen_Hou E14081185

# 2021-09-24 (WEEK 2)#

* I still feel a little out of the loop in our second class.
* I'll try my best to keep up with my classmates.
* I finally contact with other member of our group.
* I used to hear of GIT, but I actually didn't use it. It's a good chance to learn about it.

# 2021-10-02 (WEEK 3) #

* A member of our group had dropped before our group meeting started, which means I lack a partner who can use the same language to collaborate. Fortunately, there is the help of Google Translate, I will try my best not to hold our group back.

* My partner is a nice person, we have successfully booked a fixed group work time after this class.

* The third class is about fake news and some details about citing statics we search on Internet.

* Unfortunately, the 3rd URL (https://newsvoice.com/) in the task after this class is broken, so that my partner and me can't answer it.
* A member of our group had dropped before our group meeting started, which means I 
  lack a partner who can use the same language to collaborate. Fortunately, there is the help of Google Translate, 
  I will try my best not to hold our group back.
* My partner is a nice person, we have successfully booked a fixed group work time after this class.
* The third class is about fake news and some details about citing statics we search on Internet.
* Unfortunately, the 3rd URL (https://newsvoice.com/) in the task after this class is broken, so that 
  my partner and I can't answer it.

# 2021-10-10 (WEEK 4)#

* The presentation of group 5 is interesting, especially the picture that Lev Kamenev is erased.
* Before today's class, I only had a vague understanding of the financial system. The inflation is something annoying but difficult problem.
* I had heard of QE because of the news about Fed's QE I accidentally browsed last year , but just had a simple concept that QE = print money.
* The line chart professor presented gave me the first intuitive experience of the depreciation of the new Taiwan dollar.

# 2021-10-17 (WEEK 5)#

* The first film reminds me of a movie I watched during a semester German class with my friends in high school: "Die Welle".
* Herd mentality is something to be wary of. Sometimes it becomes a hotbed of extremism.
* Belief is an interesting thing, usually influenced by the family of origin. It already exists in human culture.

# 2021-10-30 (WEEK 7)#

* People who has depression still desired interaction with other people.
* Treating them commonly, or it can become pressure to them.
* Listening is important. Listening might still has chance fail to avoid suicide, like the bridge police, but it would be better than inaction.
* Depression is a common things, we don't need to refuse it if we are suffuring depression. Receiving treatment calmly. Research indicates that 70% people get better after receiving treatment.

# 2021-11-13 (WEEK 9)#

* There was some chaos this week. Everyone was busy with midterm exams and finally completed our large group assignments before the deadline.
* We exchanged projects with the super group 2. After searching the information and editing the ppt together, we choose to draw lots, and I was selected to be responsible for presenting.
* When recording a youtube video, I chose screen recording. However, the 4:3 slides will have black borders, so I temporarily changed the ppt ratio to 16:9, but its results seem to be unsatisfactory.
* The professor mentioned that he was surprised why we did not quote the WHO standard. This is indeed our negligence. We chose the "European Heart Journal" and other professional research data, but when we introduced the basic standards, we forgot to follow the same standards.
* It was a very special experience that my presentation video was played in class, although it made me awkwardly reduced the volume of the computer and tried my best to ignore the sound of my own presentation.

# 2021-12-10 (WEEK 13)#

* Our presentation mistook the BBC for American media.
* No media is objective, since they need clicks and revenue.
* I am not very clear about the super group task, maybe I will discuss with the group members when meet.

# 2021-12-19 (WEEK 14)#
* Unfortunately I cannot attend this week class due to severe gastroesophageal reflux disease.
* I encountered some difficulties in grouping. Fortunately, I finally found the group with the assistance of the TA.
* Surprisingly, I am in the new group and the original group members are in the same group again.

# 2021-12-24 (WEEK 15)#
* The third industrial revolution is still underway, and now is a crucial decade.
* In the youtube video shared by my classmates, it is very interesting for me to turn physical objects into apps and only a computer and a mobile phone are left.
* They mentioned paperlessness, but it’s a pity that only part of my notes comes from note-taking apps in my smartphone. Some of the huge number of calculations make me still rely on a large area of calculation paper. The largest tablet of our family is only the size of B5.  In my opinion, it is not a sufficient area for calculating paper.
* I choose the optional task, and I select the second video. When we are unmotivated, we can just leave a highlight messge to ourselves, after taking a break we can try it again. The difficulty make us unmotivated, but as we start, we will have chance to enter the state. 
* I had this kind of experience not long ago. When I was preparing the subject, Modern Control Systems, I was stuck in a chapter, root locus, because I was asking for leave for a class at that time. Even though I borrowed notes from my classmates, I still had nothing to do with the lecture notes provided by the teacher. Cluelessly, I flipped through textbooks and even tried to find instructional videos on YouTube. This situation lasted for two days from Friday. After I took a normal day’s rest and got a good night’s sleep, I woke up on Sunday, smoothly figuring it out!

# Diary (WEEK 16)#
* 2021-12-31
* A. Unsuccessful& Productive
* B. I felt unsuccessful because I wake up at 8 AM, inefficiently solving only 4 math problem in 30 minutes. I felt productive since I search the solution manual for the textbook which will be used on the open-book final exam on Monday. 
* c. I need to do something to renew my brain, so that I won't wake up early on holiday but do little things. Maybe I can do some sport first tomorrow morning.

* 2022-01-01 
* A. Unsuccessful& Inefficient
* B. Not only did I not exercise today, but I also got up late and felt very bad spirits.
* C. The progress of the exam was a little slow, and only half of the chapter progressed.

* 2022-01-02 
* A. Successful& Inefficient
* B. I finally finished my prepare for exam on Monday.
* C. I woke up early, even though I arranged a seemingly insensitive task for myself, I wasted half an hour to retrieve the reference materials of the wrong chapter, so that my next arrangement, writing math homework, was delayed to my lunch time. 

* 2022-01-03 
* A. Successful& Efficient
* B. I am preparing for the exam in the right direction. Today's final exam went well. My task today went well, too.
* C. I think the pressure on my final presentation can be less. 

* 2022-01-04
* A. Unsuccessful& Inefficient
* B. I felt unsuccessful since I felt that I did nothing today. I set aside one night for a group meeting with classmates. After I got home, I was so tired that I did not complete the exam review originally scheduled.
* C. I have been in class the whole day from 9 o’clock in the morning, and the rest of the time has not progressed to the level I expected. I should have practiced one-third of the exercises in my textbook.

* 2022-01-05
* A. Unsuccessful& Efficient
* B. I felt unsuccessful because my test scores did not meet my expected goals. The quiz on Friday made me feel pressured. I had to get a perfect score in that quiz, otherwise my final exam would be very dangerous. 
* C. I have review my quiz on Friday for one round, but I still have some problem need to solved. I have practiced repeatedly for the circuit diagram that the teacher is required to draw for the final exam tomorrow, and the rest will need to be reviewed for the second time before the 8 o'clock exam tomorrow morning at 6 o'clock.