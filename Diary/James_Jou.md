This diary file is written by James Jou E34095036 in the course Professional skills for engineering the third industrial revolution.


# 2021-09-25 #
* Learnt about GIT and how to use it
* Introduced to the professor and his background (both personal and educational)
* Got an insight on conflict escalation chart and its outcome
* How to sort conflict without causing more damage


# 2021-09-30 #
* Learnt on how to cite for our presentation (journals, website, etc)
* Detecting fake news
* How fake news are able to creat chaos in our society and drives people's opinion

# 2021-10-07 #
* Learnt on what gives money its value. 
* Learnt about how our financial system works
* Money is created by the bank when we are taking loans
* Bank has the power to create and print more money, however, extreme inflation can happen more money is created if there is a low demand for the currency
 
# 2021-10-27 #
* Learnt about facism 
* Which side is supported when the Taiwanese government rolls out the stimulus vouchers

# 2021-10-24 #
* learnt on how ECG can detect anxiety and nervousness 
* Learnt on how physical activities may improve our brain health
* Learnt on how our living conditions will impact our health both in a good way and a bad way
* Learnt how our surroundings may impact our anxiety

# 2021-10-28 #

* Learnt about how to communicate with those with depression
* Learnt on how suicidal thoughts may have impact on our lives
* Our close ones will me impacted when we suicide, hence if we want to do it, we really need to consider it
* Suicidal thoughts will come usually when we no longer see hope in our lives and yes I have experienced this myself

# 2021-11-4 #

* Found out that some of our classmates had ever dealt with severe depression and anxiety. 
* Some students including Taiwanese had point out that NCKU does not care about their student's mental health only until 3 people commited suicide in the course of 1 week
* A lot of students in this course were actually showing depressive symptoms 
* Learnt on how to deal with toxic work environment
* PAY < Well Being

# 2021-11-11 #

* It's a special "holiday" however all of my courses were still resumed as normal lmao
* Learnt that noise pollution can impose significant problem in our daily 
* "Green" building materials are not commonly used as no one market them
* Practices that harm the environment should be reduced in order to combat climate change.

# 2021-11-21 #

* No lecture this week! Used the time to watch the videos for the mini group homework

# 2021-11-28 #

* First offline class this week. It's great to meet up with some friends
* Learnt that if we are alone, no rules are needed
* If there's a crowd of people, a set of rules must be created to maintain order
* It is very hard to reach a consensus

# 2021-12-02 #

* Learnt that we are grtting more dependant to our phone and social media 
* Unfortunately I wasn't paying that much attention to the class as I had an exam later that night
* Just learnt thst there are no laws regarding underground constructions in Tainan
* Smoked a really good cigar after the exam. The cigar was a Davidoff Winston Churchill

# 2021-12-09 #

* Learnt that every country's news have different stand point on the exact same news.
* Wasn't able to give full attention to the class as I had an exam the next day and felt way too anxious.
* I do feel confused on how are we going to do the big group project for next week
* Overall it's an interesting topic that we learnt today

# 2021-12-16 #

* A different style of presentation today as the presenters were sitting up front
* There was a debate on the topic 
* The group with the least vote or considered impactless was disbanded and told to switch groups
* My group  got 2 new members

# 2021-12-23 #

* Successful 
* I was able to hit the gym even after a night out with some friends
* I was also able to study for a while
* Try to get up early

# 2021-12-24 #

* Unsuccessful
* Woke up with an extreme headache 
* Still went to class but I was not paying attention at all
* Skipped the class on 3-6 pm as I was asleep. However, it did nothing to cure the headache
* Went for Christmas Eve barbeque with friends
* Need to wake up early the next day

# 2021-12-25 #

* Successful 
* Merry Christmas
* Was able to get up early to prepare myself to go to Kaoshiung 
* Attended Christmass Mass in Holy Rosary Cathedral Minor Basilica as I was invited by a friend (I'm a Christian)
* It was my first time attending a Mass.
* Helped out in the Christmass celebration as part of the decoration team and  monitor engineer.
* Distributed the remaining food boxes to those in need (the homeless)
* Try to go to the gym tomorrow

# 2021-12-26 #
 
* Successful
* Finally hit the gym after 2 days of rest days
* Still felt a sharp pain on my shoulder when doing dumbell press
* Went to the movies with a friend and we watched the new Spiderman movie
* Hopefully I will be able to wake up early tomorrow and go to class.

# 2021-12-27 #
* Unsuccessuful 
* Unable to wake up early to go to class
* However I did catch up on my paper work for my thesis 1 class
* Hopefully I'll be able to wake up early the next day

# 2021-12-28 #
* Successful
* Was able to wake up early and attended my morning class
* Need to study as I have an exam tomorrow

# 2021-12-29 #
* Unssuccessful maybe
* The exam was total BS lmao. 
* Managed to attend my morning classes as I have to submit a report
* Hopefully I will be able to wake up tomorrow to attend my morning classes.

# 2021-12-30 #
* 5 ways to be successful
* Do what you love
* Love what you do
* Make sure you do your best on what you love
* Keep your circle of friends small
* Trust yourself
* Was pretty productive I guess
* Happy new year and yes I went drinking on new year
