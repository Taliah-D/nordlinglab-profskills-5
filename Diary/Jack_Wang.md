This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Martin Wu E1312000 in the course Professional skills for engineering the third industrial revolution.

# 2019-09-12

* The first lecture was great.
* I am really inspired and puzzled by the exponential growth.
* I don't think exponential growth applies to food production.
* Why are houses becoming more expensive when all technical products are becoming cheaper?
* I think everyone should follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).

# 2019-09-19

* The first lecture was great.
* I am really inspired and puzzled by the exponential growth.
* I don't think exponential growth applies to food production.
* Why are houses becoming more expensive when all technical products are becoming cheaper?
* I think everyone should follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).
