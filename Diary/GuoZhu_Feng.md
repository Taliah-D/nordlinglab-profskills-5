This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Martin Wu E1312000 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

* The first lecture was great.
* I should be more careful about where my information from.
* I should notice how the data was collected.
* I have to think twice before I accept any online informations.
* I am surprised about fake data influence.
# 2021-10-07 #

* The second lecture was a little borring because I had seen Steven Pinker's TED talk.
* Why are houses becoming more expensive when all technical products are becoming cheaper?
* I am really inspired and puzzled by the exponential growth in data and narrow AI.
* The [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) is helpful.
* Now I understood that I should write the diary entry for all weeks in this same file.
# 2021-10-14 #

* The first lecture was great.
* I should be more careful about where my information from.
* I should notice how the data was collected.
* I have to think twice before I accept any online informations.
* I am surprised about fake data influence.