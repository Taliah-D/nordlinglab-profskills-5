This diary file is written by Leo Tsai E14083137 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* This course operates in a whole different mannar than moodle, so it'll take some time for me to keep up the pace.
* I really learnt a lot from the exponential growth topic last week, hoping I can develop valuable skills along the course.
* I think exponential growth in green energy will lead to a world of abundance and everlasting.
* Glasl conflict escalation model subdivides human interaction into multiple stages, which is a novel idea to me.
* Scrolling on google to find out whiat GIT is, and found out Linus stated that git can be interpreted as anything, depends on your mood.


# 2021-09-30 #

* I find this course not as stressful as other courses, and we can also cultivate invaluable skills rather than academic knowledge in the mean time.
* A good statistic can be visualiaed with vivid diagram or gragh rather than cold numb data.
* In statistics, scale is everything. Once you change the scale(axes), you can change the story.
* I learned how to cite articles correctly today, which is definitely useful in all directions.
* Newsvoice topic: How to be free from mental conditioning caused by society
* Humans copy what other people are doing and it happens through repetitive content.
* The average person thinks that they get married at a certain age.
* Take a nine-to-five job instead of starting one's own buissness.
* The subconscious manufactures your behavior through analyzing the repetitive content in your environment.
* The solution is to hack your subconcious mind and exchange bad habits with healthy one.
* It's mainly a repetition fight.
* Cited from https://newsvoice.se/2021/10/jason-christoff-mental-conditioning/


# 2021-11-04 #

* Professor talked about financial system today, and introduce some interesting facts about deposits and loans.
* Gold is often one of the assets that is considered to be the ultimate safe haven, which value usually doesn't fluctuate dramatically.
* If one bank's deposit-to-loan ratio is greater than 1, which implies that the bank has loaned out every cent of its deposit.
* It is the danger zone because it has no reserves to pay customers for demand deposits.
* It's interesting to learn that whenever Chinese's new year has come, the amount of money government held increased drastically
* due to the Chinese's envelope custom.
* I also learned a tricky word "fungible", which is something may be replaced by another equal part or quantity.
* Money is the typical instance that is fungible.
* The purchasing power of each currency has droped exponentially for the last hundred year.
* The inflation of currency has become so severe that I can't help wonder what can we afford to buy
* with a thousand dollars in the next or two decads to come, perhapes not even a drink.
* If the government keep letting the inflation sabotages our currency purchasing power, then we must publish a new currency.
* The wealth gap has arised since 1980, and continues to grow with an exponential manner.
* Every politician tells that we must need to fix this gap, and makes promises to their voters.
* However, who really commited to their promises after elected, who ever really has taken this issue seriously.
* Tragedies will still happen, and the gap will grow faster and faster until the poor can't withstand the extremely uneven 
* distrubution of resources.

# 2021-10-14 #
* I gave a presentaion today, and recieve a lot of advice from professor.
* I used to think that giving presentation in English is quite intimidating, however now I think that only be overly prepared can make my nerve relax. 
* Facism is used as a kind of general purpose abuse or they confuse facism with nationalism.
* Facism tells me that my nation is supreme and that I have exclusive obligations towards it.
* Now data is replacing both land and machines as the most important asset.
* Facism is what happens when people try to ignore the complications and to make life too easy for themselves.

# 2021-10-21 #

* Today's vedios are very interesting, and energetic. Explaning how people can be brilliant and toward a better version of ourselves in scientific way.
* Often people think that changing behavior will change the result, howevert it's not the case.
* There's 4 levels beneath behavior, which are thinking, feelings, emotions, and physiology from top to buttom.
* The most enchanting part is Dr.Alan explains that emotion and feelings are completely different things.
* "Emotion" is energy in motion, and those energy is come from physiology, including all the signals transmitted by our senses.
* In conclusion, emotion is all the signals inclusive of electric signal, pressure wave, and son on.
* feeling is the awareness of those energy flow in our body.
* In order to be brillient everyday, we must start to control those levels beneath behavior.
* Exercise is truly the key to a healthy life, working out consistently makes our brain release adequate amount of endorphins
* Endorphins can not only reduce the perception of pain, it also triggers a positive feeling in your body, such as boosting mood.
* I build a habit of exercising regularly in the past 3 years, it was hard to embrace the soreness accumulated in muscle at first.
* however, as endorphins began to release, I started to enjoy the process over pain.
* After all the hard work I put in, I realized that my body begin to shape, and become more fit than ever, which bring me sense of achievement.
* Now, I'm more confident, and proud of being recognized as a disciplined person, moreover, an atheletic person.

# 2021-10-28

* Today I volunteered to present in the super group, which was a brave move to me. I recalled that professor said
* "Only by pushing yourself out of comfort zone, which is presenting publicly can you overcome the anxiety of public speaking."
* Therefore, I must not hesitate to say yes when there's oppertunity to present. As awkard as it may be in the begining, I'll get good at it eventually.
* People often get terrified when thinking about talking to people with depression.
* The reason is that people think that they might put their foot in it, and accidently make the situation worse.
* However, it is not the case. We can be a listener to depressed people, perhaps bonding with them will make us feel great.
* Besides, we might learn something from them. Depressed people are people too, they are not monster, their brain just don't function normally like us.
* As a successful black woman, Nikki Weber suffered from depression and what's worse was that she thought having depression is a shame.
* Until her beloved nephew commited suicid beacuse of depression, she finally couldn't stand the pain of not speaking out the feeling of depression.
* She began to resort to doctors, talk to family and friends, eventually she overcome the depression she was ashamed of.
* Nikki even started to share her own experiences and taught us how to fight mental illness.
* Mental health is as important as physical health, and they complement each other.
* I think I have sort of experienced depression when I was in high scool beacuse of the incompetence of handling pressure.
* However, I learned some valuable lessons from this hard time, which is "Everything will be fine in the end."
* Nothing is there to stop you from doing the things that you want except your own fear.
* Fears are fabricated and are conjured up, and are lies. Most of our fears won't even happen, we're just too stupid to scar ourselves!


# 2021-11-04 #
 
 * Having a meaningful life can make people more dedicated and more resiliant, and they do better at work or school and even live longer.
 * Belonging, purpose, transcendence and story telling are the four pillars for a meaningful life.
 * Happiness comes and goes, we can't rely on the persuit of happiness as our goal of life, there's going to be something that upset us.
 * However, having meaning in life make us have something to hold on to, we can rely on it no matter we're mptivated or depressed.
 * Meaning fills the emptyness in our heart, make us more resilent to adversities.
 * As time flies, I've become a junior in college, I'll soon have to enter the workforce, but all I have now is knowledge in books.
 * I literally have no idea about how to apply the academic knowledge to work, I can only calculate math, solve physics problems and draw free body diagram.
 * I have little to noun social skills such as communication, critical thinking, and so on.
 * Toxic work environment will do one no good, if one is working in a toxic environment just to get hgh salary, then it's the stupiest idea.
 * You can't spend your money if you don't have good health, well, except on your medical bill.
 * My solution to a fullfilling job is as followed: 
 * Work on myself hard to enrich my kowledge and skills, therefore I'll grab the oppertunity once it comes.
 * Stay consistent on the promise I make, and be relentless to persue my goal.
 * Never settle and relax, need to keep grinding, and hustle beacuse that's the difference between greatness and mediocrity.


# 2021-11-11 #

* Noise pollution is common nowadays, no matter it's heavy traffic or construction sites, exposing oneself for too long is harmful to our body.
* Exposing ourselves to noise pollution continuously will reduce our life span and either develop mantal illness or physical diseases.
* Using steel reinforced cement is creating lots of CO2 emissions as well as producing, transporting, repairing and daily consumption.
* Energy consumption of steel reinforced cement building is larger than others since people need to turn on air conditioner to cool down the temperature inside.
* It's cool to learn that green magic building awarded world's greeenest building, and this building is in NCKU!
* I also learnt that using natural materials such as woods for building will not reduce CO2 recycling.
* Making woods as our building prevent us to generate more CO2 emissions further.
* The course project topic for our group is methods to empoer people, we found it hard to understand at first, however we managed to overcome the obstacles.


# 2021-11-25 #
* Today is physical class, initially I felt reluctant to attend the physical cousre since online course is more convenient and comfortable.
* The new classroom is astounishing, and I was amazed by the number of monitors, theree are two in front, two in back and four aside.
* All monitors are synchronized to the professor's labtop.
* Mechanical department seems to have earn a fortune lately.I'm thrilled to see our depaetment has a state-of-the-art equipment installed in the classroom.
* Today's vedio watch topic "How we need to remake internet" by Jaron Lanier is fantastic, Lanier is the pioneer of the virtual reality and a computer scientist as well.
* Lanier said that we, humankind are responsible for the distorted values nowadays.
* Another vedio watch "Millenials in the workplace" by Simon Sinek really illustrated how and why children in this generation has gotten so unsympathetic and indifferent.
* Biggest problem among this generation is the lack of authenticity since the absence of real interactions with people, deep connection is seldom formed.
* The world seems to have connected everthing through the internet, however, people relationship with each others has become less bounded than ever.
* When ever we encounter any adversity, we choose to scroll the internet instead of talking to real person for real advice.
* Besides the internet problem, the way how parents adore thier children is the main reason why suicide rate is climbing.
* Parents nowadays have over protected thier children, being a helicopter parents who always clean up choldren's mess for them.
* This ends up letting children incable of dealing stresses on their own, once they face any difficulties, they choose to embrace instant gratification as an escape.
* Today we had in person discussion during the course, which is the reason why I've changed my mind that I now do believe physical class is way more better than the online one.
* I'm more involved and engaged in the group discussion, and the efficiency is 10 times faster than the online course.
* This leads to the point where Simon said real bounding is formed through real interactions and communcations, I couldn't aggree to it anymore as I attended today's physical course.
* To sum up, this course is definitely in the top of the list of my favorite classes that I've taken in this semester
 

# 2021-12-9 #
* Today I got picked to read the diary for the first time, however, I forgot to write one last week, which made me extremely awkward. Therefore, it's better do write diary as soon as possible.
* If professor would be so kind to emial us when is the right time to switch group, then no one like Hank Lee would be abandoned as this week situation.
* Sweden media has strong self-imposed censorship, so in the news diversity ranking, Sweden is at the bottom of the ranking, completely opposite to the free media index ranking.


# 2021-12-16 #
* I really think professor's idea about noise detection project is too demanding and a little unrealistic, here is the reasons.
* First, how are we going to collecct the money for 50 smartphone devices even if the cheapest smartphone takes about few thousand NT dollars! 
* Second, professor ask us to build a website to observe the noise distrubution in the locations where our devices installed in the campus.
* As far as I'm concerned, this is far beyond our capability as we still have tons of studys, tests, works waiting for us to tackle. I personally think We literally cannot mangae to finish it while having others workload.
* I still think that professor or TA should try to email students the overview of the given tasks as a reminder, not just verbally deliver the tasks. 
* Most of the professors I've met so far always send notifications of the overview about the important tasks given before or after the lecture via moodle or mail.
* This Saturday our country held a referendum. Here's four issues.
* 1. Restarting the nuclear power plant or not? 
* 2. Should we stop the construction to protect algae? 
* 3. Should we open up to American pork containing ractopamine? 
* 4. Should elections along with referenda be held together?
* All proposals are denied by the majority of the citizens in the end, to be honest I was quite disappointed about the results.
* Why not restarting the nuclear power plant to generate enough electricity to provide the rising need for electricity since it's getting hotter in the summer, and all the tech-componany that require lots of power to manufecture products.


# 2021-12-30 #
* Still til this day, I still wonder how are we going to finish the course project and there's only two weeks left but we haven't done shit.
* The topic professor assigned to us is way too demanding for us. How on the earth would professor think that we must have the acquaintance who happens to work in a sound lab!
* Professor wants us to find a cheap device(smart phone) to detect noise and it must be placed continuously for days to collect noise data.
* One of the biggest problem we may encounter is the power supply problem, how do we make sure that the battery of the device doesn't die during the working hours.
* Actions to fix power supply problem: 1. Mobile charger. 2. Extension cord. There's a great chance that the charger will be stolen if we take the first action. The problem of the second action is that where do we find plugs for cords?
* As the final approaches, despite the fact that we have to dedal with tons of tests and papers to submit, we, the hybrid group members still have to face with the uncertainties and difficulties of the course project.
* To be honest, I fell like giving up upon this course project. Considering the cost to set up smartphones for noise detection even if using the cheapest one is a lot, not mention about the power supply problem. The chance we complete the course project is bleak and the pain is endless.
* I never would have thought about giving up, DAMN!!! I'm never a quiter, but I really don't know how to deal with this tricky and meaningless topic, someone come to save me, it's torture! Set me free, Nordling!

# Daily Dairy For a Week (About Successful and Productive) #
Explanation :

-A. You need to label each day as either Successful / Unsuccessful and Productive / Unproductive.

-B. Analyse why you felt Successful or Unsuccessful and productive or Unproductive.

-C. State one thing you will do different the next day to be more Successful or Productive.

* 2021-12-30(Tursday)

   -A. Successful and Productive.

   -B. I just got out of the hospital, however, I still managed to work out in this afternoon. I'm delightful that I finally can get back to the normal life after being hospitallized for am entire week.
    I study hard in order to compensate the study I was missing.
    
   -C. I want to quit the habit of using phone before going to the bed.
   
* 2021-12-31(Friday)

   -A. Successful and Productive.

   -B. I woke up early and made myself a decent breakfast and then I headed to the library. I studied for 7 hours today and that was amazing and productive.
    And I went with my family to watch the Spider man: No way home, it was amazing since the movie gathered the 3 spider men from the 3 different universes in the same time!
    Spending times with my loving family is a blessing, and I really enjoy it.
    I also did a work out today. 
    
   -C. Today was perfect. I got the work done and also spent time woth my family, there's nothing I would like to change in this day.

* 2022-1-1(Saturday)

   -A. Successful but unproductive.
   
   -B. Today I went to a light travel with my family since it's the beginning of 2022, we dicided to travel around. The anxiety of the final exam sneaked up to my mind from time to time, I still enjoyed the journey though.
       I also did a work out today.
       
   -C. I should wake up early to study first.
   
* 2022-1-2(Sunday)

   -A. Successful and productive.
   
   -B. Today I come back to Tainan and say goodbye to the holiday cause the shit is about to get real. The final is approaching, gotta start preparing.
       I also did a work out today.
       
   -C. Great day, no changing is needed.
   
* 2022-1-3(Monday)

   -A. Successful and productive.
   
   -B. I decided to study in the NCKU study center. I found out that I'm more efficient and productive here.
       I also did a work out today.
       
   -C. Great day, no changing is needed.
   
* 2022-1-4(Tuesday)

   -A. Successful but productive.
   
   -B. I studied for 8 hours today. NCKU study center is so crowed that I even had to share a desk with another person. 
       I also did a work out today.
       
   -C. I need to be more focus and present while studying. It's about how deep you can study in, not how long the time span of the studying, so gotta be more cautious when distracted.
   
* 2022-1-5(Wednesday)

    -A. Successful and productive.
    
    -B. I found online courses for computer organization, which is the subject has been bothering for weeks. However, this online course has really explained the content well and making it understandable compared to my professor who gives the lecture.
        I did a good cardio today.
        
    -C. Great day, no changing is needed.
    
* 2022-1-6(Thursday)

