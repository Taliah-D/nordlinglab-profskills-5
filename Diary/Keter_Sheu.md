# 2019-09-19

* I am glad to have an opportunity to see git and markdown again, and to have a further comprehension of them. However, I am a little surprised that we used bitbucket instead of GitHub, since I used to believe that the latter one is more popular.
* It is such a coincidence that my topic of the presentation, lithium-ion battery, is an example of the lecture, and I believe that I went to the same website as professor did. I was confused about the Ford's model T when reading the website, and I am happy that my problem was solve during class.
* The video talk about so many information that I cannot completely understand the whole content. I guess I had to watch it again to be able to prepare for the presentation.
* I am looking forward to knowing my new partners, and hope to discuss with them as soon as possible

# 2019-09-26
* It is really a pity that I did not have the chance to participate in the lesson of fake news, since the world is filled with useless and harmful news and I am really interested in it.
* It had been hard time to finally get settled in a group for the 1-4 week’s group. Since I was rearranged twice to finally have a group member, unfortunately I didn’t have the chance and time to work with my partner this time, and ending up doing the homework in a rush in this morning. I suggest that the students next year can find our own groups for the only first time, so they may be less likely to work on their own.

# 2019-10-03
* I am not familiar with the bank systems, and I am glad to know more about them.
* Deposits and loans are only records of the banks.
* Large banks can easily cause inflations.
* Productive loans is okay for the economy.
* The economy propelled by governments, central banks, banks, and citizens.
* Learning how to manage the illusion-like money is important.

# 2019-10-17
* Extremism originates from hatred, so communicating with others is important.
* Fascism is disguised as an attractive form, and people will accept it easily when they are weak.
* The fascism is a threat to the liberalism, and may be coming back in a mightier form.
* Combating the problem of CPC is walking on the thin ice since the economy in China is important to the world.

# 2019-10-24
* The first video is talking about the importance of exercising and its benefits, but I think it is common sense at least for me. 
* The reason for not exercising is just inadequate time and laziness. 
* It is pretty fun doing some exercise in class.
* The second video is about a new idea of reducing the opportunity to get sick, which is pretty interesting.
* The new idea is a little bit too ideal, and there are a lot of things to discuss.
* I have a thought of encouraging everyone or certain people to having a blood checkup by reducing the their health insurance fee, so some illness not only may be found in early stages, but also establish a complete data base.

# 2019-10-31
* Most people do not want to talk with depressed people.
* Depressed people wants to communicate with others, but their social ability has weakened.
* Lots of people suicide at the Golden Gate Bridge.
* Listen and understand.
* Chemical imbalance in the brain or gene can cause depression.
* Depression will improve with therapy, treatment and medication.

# 2019-11-07
* Our choices can be manipulated.
* Know that you don’t know yourself.
* I haven’t have such experience, so I kept a conservative attitude.
* Forgiveness is a good solution to conflict.
* There are four pillars of a meaningful life: belonging, purpose, transcendence, storytelling.
* Find the meaning of life.

# 2019-11-14
* We have more power than we think.
* Do not breach the peace.
* Cause no one any harm.
* Cause no one any loss.
* No mischief in promises and agreements.
* Need your consent before the force of law.
* If we haven’t break the law, we don’t have to talk to the police.
* What you perceive as legal does not always mean it is law, in fact very often it doesn't.

# 2019-11-21
* Negative emotion can be spread quickly.
* The companies need to be financed by a third person.
* Social media and cellphone are addictive, and may let us lose the ability of interacting with others.
* Put cellphones away.
* Algorithms can go wrong easily, and can harm the minorities.
* Bias and past example make the model wrong.
* Check the model, and consider the errors.

# 2019-11-28
* Check the news from the original source.
* Compare multiple news and tell which contents are objective.
* Check before sending to others.
* The history is quite confusing to me.
* Monetary mistakes happens repeatedly.
* Most countries make the same mistakes.

# 2019-12-05
* Events may be described differently by different media.
* Technology place pressure on the system of Earth, but it also help us realize the condition of the Earth.
* Four kinds of pressure are population growth, climate change, ecosystem decline, human pressure.
* We should not just depend on the model predicting.
* Nations should work together.
* We should change our minds to thrive together this century. 
* Economy should not grow forever.
* Find the balance point.
* Reuse the resources.

# 2019-12-12
* The ideas from the each group is interesting.
* The debate really gives me inspirations.
* The full preparation for each topic is quite difficult.
* It is a pity that there is too many topics to discuss deeply.
* I am looking forward to seeing the differences between results of the voting.

# 2019-12-17
# 19/12/19 Thr
* A. productive and successful 
* B. I felt productive and successful because I have a task tomorrow, and I was really concentrated.
* C. I will make a work schedule.
# 19/12/20 Fri
* A. unproductive and successful 
* B. I felt successful because I did a great job on my task. I felt unproductive because I was tired to do other things.
* C. Relax for restoring energy.
# 19/12/21 Sat
* A. unproductive and unsuccessful 
* B. I felt unproductive and unsuccessful because I don’t have enough sleep for days.
* C. I will try to sleep earlier.
# 19/12/22 Sun
* A. unproductive and unsuccessful 
* B. I felt unproductive and unsuccessful because we took photos for graduation, and I felt so tired to do my assignments.
* C. I will urge myself to arrange my time tomorrow.
# 19/12/23 Mon
* A. unproductive and unsuccessful 
* B. I felt unproductive and unsuccessful because I was stuck with my coding project, and found the problem very late.
* C. I will discuss with my groupmate.
# 19/12/24 Tue
* A. productive and successful 
* B. I felt productive and successful because I had a test and a presentation tomorrow, so I had pressure to prepare for them.
* C. I will keep giving myself pressure.
# 19/12/25 Wed
* A. unsuccessful and unproductive
* B. I felt unproductive and unsuccessful because I didn’t get a satisfactory grade on my presentation, although I think I did it well, which made me disappointed all day.
* C. Don’t care the result so much.

# 2019-12-26
* Everyone shared five rules.
* Every group presented their topic.
* We have to brainstorm ideas.
