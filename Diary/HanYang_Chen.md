This diary file is written by Hugh Chen E14086282 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23
* I am still learning how to use this GIT tool. Seems to be easy, but quite difficult actually.

# 2021-09-30
* I've been injecting the vaccine for the covid-19, making me so uncomfortable to pay attention in class.
* It's a pity to barely learn anything because of the situation, but I really can't control.

# 2021-10-07
* Introducing about financial system, deposits and loans.
* Gold is considered to be the ultimate safe haven.
* When a bank's deposit-to-loan ratio is greater than 1, the bank has loaned out every cent of its deposit.
* "Fungible"
* The purchasing power of each currency has dropped exponentially for the last hundred year.
* The wealth gap has arised since 1980, and continues to grow with an exponential manner.

# 2021-10-14
* Fascismo-法西斯主義
* "Extremist"
* Human are the most imaginative.

# 2021-10-21
* How you think affects how you do. How you feel affects how you think. Your emotions affect how you feel. Your physiology affects your emotions.
* Seems to be easy to control people if learn enough about psychology or the brain.
* Exercising improves your brain.
* Exercise 3~4 times per week at least, with 30 minutes every time.
* True health care system could save us from unnecessary costs and risky procedures.

# 2021-10-28
* We were assigned into a super group in class. I can strongly feel the differeance between a small group of three people and a big one with more than ten. The progress of discussing is much slower although we can hear more different opinions from others.
* From a presentation from another group, I learned the importance of staying away from cellphone before going to bed and sleeping enough. The former leads to a better sleep and the latter benefits my body in many ways.
* From the TED talks, I learned that to listen is a good way to interact with a depressed person. I agree with that because a friend of mine, who suffers with depression, told me that he felt great when talking to me. Although I actually didn't say anything but just listened during the chat.

# 2021-11-04
* When trying to make yourself happy, the process might actually make you more anxious. I can kind of feel that when I feel happy playing games all day, but also feel anxious for wasting so much time on playing.
* I took the second BNT vaccine this weekend at Cheng Kung University Hospital. It feels really different compared with the first time at a small clinic. The manpower is sufficient and the moving line is clear. This time, I only spent less than 20 minutes on the whole process, while last time at the clinic, I used more than 30 minutes just waiting in line. This experience makes me see the difference when working in different resourses.

# 2021-11-11
* 躺平主義(lying flat)- not to keep fighting, but to relax and simply stay alive(?
* This week when I got into a bus, there was no other seats, so I sat on the priority seat. But when I tried to yield the seat to those who actually needed, they rejected. It made me think if I should sit there in the first place.

# 2021-11-25
* When discussing about the group project with the topic of Rights to Petition Government, I just notice that there are several different ways to give suggestions to government in Taiwan.
* I subscribed Disney+ channel and watched The Greatest Showman this weekend. The movie presented many social problems from late 19 century, which I think aren't totally remaved until today. 
* "The noblest art is that of making others happy." -P.T.Barnum

# 2021-12-02
* Everyone is responsble on stopping fake news from spreading out. So always check for the source before sharing a news or fact.
* This week, many groups did the project based on noise pollution. I also think that is a big problem that can be discussed since I live near the railway and beside a elementary school. The noise made by them are so loud and annoying.

# 2021-12-09
* During the research from the last presentation, I feel like news from some certain countries are not that easy to acquire or the news are really simplied.
* People from those countries might not be able to think by themselves for big events because they don't even know enough about the events. This might be a great way to control people's thoughts, by controlling the media, but I think it actually makes people blind, which is a bad outcome.

# 2021-12-16
* One of the groups was talking about 顏清標, who is influencing in Taichung. I am from Taichung, and I have heard rumors about him too, feel like I am lucky to survive in Taichung until now?
* In the class, there was a chart saying that zero textbook mentions that having less kids saves the Earth. I think that is reasonable since if people actually have less kids, booksellers will have less customer.
* Professor was suprised that no one mentioned about less kids emmits less carbondyoxide while our generation actually has less kids. I think it's normal cause maybe most people have less kids not to help the environment, but to save money or something.

# 2021-12-23(Thu)
* Feels successful and productive today.
* Finished reviewing two chapters of Mechanics of Materials, and had a exam today. Also, achieved the goal I set in Brawl Star.

# 2021-12-24(Fri)
* Feels unsuccessful and unproductive today.
* I woke up late and barely did anything.

# 2021-12-25(Sat)
* Successful but unproductive.
* Finally woke up early and was not late for any friends gathering.

# 2021-12-26(Sun)
* Successful but unproductive.
* Travelled around Koahsiung, planned so well.

# 2021-12-27(Mon)
* Feels successful and productive.
* Finished homework, conquerred exam, and slept more than eight hours.

# 2021-12-28(Tue)
* Feels unsuccessful and unproductive.
* I was so tired and nearly slept all day. Then I lost about 200NTD playing mahjong.

# 2021-12-29(Wed)
* Feels productive but unsuccessful.
* I set couple of alarms but just couldn't wake up. But I still finished the tasks I wanted to finish.

# 2021-12-30
* After a week of writing diary, I kind of can find of ways to success.
* Improve a little is better than trying to learn a lot one day.
* To notice my weakness is important too, so I can try to fix the problem.