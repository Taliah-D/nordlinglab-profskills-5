This diary file is written by Gabrielle Vania Tandjung H24075332 in the course Professional skills for engineering the third industrial revolution.  

# 2021-09-23 

* The Professor still discussed about conflicts on the second lecture, we were given more examples 
* There were some presentations on the second period, the Professor gave some inputs for improvements 
* The Professor also taught us how to use Github and how to make the diary properly 
* On the third period, the class discussed about the 4th Industrial Revolution that I didn't know about before 

# 2021-09-30 

* This week we talked about how to differentiate fake and real news 
* We also watched a video talking about how to spot bad statistics 
* As a Statistics student myself, that video was very interesting to watch 
* The professor talked again about diary and grouping problems since there are still some students who do not understand, me included 
* My group have not gotten the chance to present yet 

# 2021-10-07 

* This week we talked about economy and finance 
* The professor asked us to fill a questionnaire about economy and filling that made me realized that I have forgotten a lot of things about economy 
* Then we discussed more about the international and global economy situations 
* All of my previous group members dropped this class so I had to do the presentation for week 4 all by myself 
* Thankfully we are going to have new group arrangements starting next week

# 2021-10-14 
* This week we talked about humans and social lives and technologies 
* The will always be different social standings and powers inside the society 
* And we will have to be able to control it in order to prevent and avoid dictatorship 
* In terms of technological advancements, the technology are advancing in a fast pace during wars, which of course is extremely beneficial 
* However, technologies need to use wisely and we should always be careful when combining it witn human lives 

# 2021-10-21 
* This week we talked about how to live a healthier life 
* We also talked about stress levels and axiety and how they hinder our flow of works and ability to think
* There are ofcourse many ways to live healthily 
* However those claims have to have legit supporting evidences 
* Thus we have to keep our mindset stable in order to be able to live a healthier life 

# 2021-10-28 
* This week we talked about depression 
* We watched several TED videos regarding depression itself and people with depression
* It is said in the video that we should treat those with depression like usual and just talk like usual 
* We do not need to force ourselves to talk to them specifically about their conditions but just being there, knowing that it is okay and safe for them, is enough
* We should also understand that being diagnosed with depression is not a symbol of weakness and that we should not suffer alone 
* I do not personally know anyone that has depression but if one day I meet someone with depression, I hope I will be able to use the knowledge I gained from these videos to handle them accordingly

# 2021-11-04 
* This week we still talked about our mental health and issues 
* Sometimes we blame ourselves for our sickness but it might be affected by our environment 
* And that fact makes climate change a very serious issues 
* In order to live a healthier life, have a better mindset and mental awareness, we need to live in an environment that is as good or even better 

# 2021-11-11
* This week we did not do presentations with small groups but with the final big group instead 
* We watched all the presentation videos that each of the teams have made and submitted 
* All of the presentations are very nice and informative 

# 2021-11-18 
* There is no class for this week 
* My group decided to have a discussion about the next presentation on the supposedly class hour 

# 2021-11-25 
* Rules are needed since we are living with other people 
* Each of us is different, our needs and thoughts are special from each other 
* Thus we need to have the consensus
* The consensus need to accomodate all of our different needs 

# 2021-12-02
* This week all group presented their ideas about making the society better that can be implemented before the end of the course 
* The ideas my group presented are a bit hard to be implemented before the end of this course 
* Because it involves the police so it might need a long time and procedure to get accepted 
* We are currently thinking of another idea that is simpler so that it can implemented before the end of the course 

# 2021-12-09 
* I personally think that the topic and tasks for this week presentation were harder
* I also think it is difficult to compare countries if we don't understand their cultures and languages fully
* Perspectives are also difficult to be defined because they are very subjective
* Most news reports are based on the authors' perspectives and are subjective, usually siding from one point of view only
* That is why the same topic can have different point of views which can result to different conclusions

# 2021-12-16 
* This week's lecture was different than the usual 
* Instead of regular presentation, the Professor asked each group to assign one person to asnwer several questions from the Professor regarding the presentation
* They were then asked to have a debate and answer further questions from fellow classmates 
* Before the class ended, we were all expected to join one group whose topic interest us the most 
* I hope with the new group, the ideas that we have all thought can finally be implemented 

# 2021-12-23 
* There was a debate in today's class 
* Capitalist is actually pretty useful since they are the one who know what people want and made it into business 
* which is very helpful for the country's economy and living standard of the citizen 
* Overall, it was a sucessful and productive day because I have several classes that I need to attend and I learnt something new 

# 2021-12-24 
* This day was both successful and productive 
* I had a meeting with one of my professors and we talked about my future prospects 
* I also successfully acquired the recommendation letter I need for my Master application 

# 2021-12-25 
* This day was not so productive 
* Since I didnt really do anything beside tidying up my room and relaxing 
* However I still would like to say that it is a successful day since I didnt feel stressed and there was no failure 

# 2021-12-26 
* This day was not so productive 
* All I did was pretty much the same with yesterday 
* However I still would like to say that it is a successful day since I didnt feel stressed and there was no failure 
* I dont have any homeworks to be done or any exams to study
* However starting tomorrow I will start reviewing some of my lessons since the final exam is coming 

# 2021-12-27 
* This day was quite productive 
* I went to class and learnt something new 
* I also took some graduation pictures with my classmates 
* Overall it was a pretty productive and successful day 

# 2021-12-28 
* This day was quite productive 
* I went to class and learnt something new 
* I also had dance practice for an event  
* Overall it was a pretty productive and successful day 

# 2021-12-29 
* This day was productive 
* Although I do not have class on Wednesday, but I am able to finish all of my homework 
* I also had a group discussion for an upcoming presentation 
* Overall it was a pretty productive and successful day 

# 2021-12-30 
* In today's class we need to present about out final topics 
* It seems like my group needs to change the topic 
* We also shared our 5 rules to have a more productive and successful day 
* I hope my days in 2022 can be more productive and successful than in 2021



