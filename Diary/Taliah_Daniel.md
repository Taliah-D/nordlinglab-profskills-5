This diary file is written by Taliah Daniel in the course Professional skills for engineering the third industrial revolution

# 2021-09-16 #

* A youtube video was provided by the professor 
* My take from the video was: a fully electric economy can change our everyday lives as cars will be cheaper and convenient.
* Learnt about professor's background

# 2021-09-23 #

* GIT sounds interesting
* This course is really going to prepare me for the professional world.
* I love the fact that we watch TED talks, they are my favourite videos to watch.
* The TED talk today was about statistics, and it serves to answer the question, "Is the world getting better or worse?"

# 2021-09-30 #

* I finally understand why we use GIT. It is because we are moving in a direction where automation is done in code and in most professions it is important to learn and understand code.
* I also learnt how to perfect citing. By adding last visited, the name of the publisher, the license, and using dashes between the dates.
* We also watched two TED talks, both were quite intriguing in the claims that were made.
* ------Homework------ Stories on Newsvoice.
* Asia (channelnewsasia.com): How Japan plans to release contaminated fukushima water into the ocean: Japan plans to release into the sea more than a million tonnes of radioactive water from the destroyed Fukushima nuclear station. The work to release the water will begin in about two years, and the whole process is expected to take decades.
* Europe (theguardian.com): Fukushima: Japan announces it will dump contaminated water into the sea: Japan announces that it will release more than 1m tonnes of contaminated water from the wrecked fukushima nuclear power plant into the sea. Confirmation of the move came more than a decade after the nuclear disaster. The prime minister said that releasing the water into the ocean is the most realistic option.
* Middle East (aljazeera.com): Japan to release contminated Fukushima water into the sea: Japan says it will release more than one million tonnes of contaminated water from the ruined fukushima nuclear power station back into the sea. The work to release the water will begin in about 2 years and is expected to take decades. The government said that inorder to comply to strict regulatory standards, they select oceanic release.

# 2021-10-31 #

* Todays class was emotional with these particular ted talks.
* One thing that stuck with me through one of these ted talks is: words are not the most important thing to focus on when dealing with depressed people.
* I actually send the link to the last ted talk to a close friend of mine who i knew was struggling with depression,i hope it helps him.
* This class has really helped me to improve myself as an individual, in terms of communication and teamwork.
