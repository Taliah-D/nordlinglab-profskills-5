﻿Diary By Jay-Lin 林志杰

# 180920 Week 2

1. Progress is not miracle, but problems solved.
2. News programs usually broadcast something bad, but is that means that's how the world is?

# 180927 Week 3

1. The definition of "War" is the problem causing statistic difference.
2. Change the scale of chart, change the story.
3. The method to face fake statistic is not believing nothing, but do your research.
4. The "Average" statistic figure cannot really show the circumstance without considering the extreme figures in.

# 181004 Week 4

1. Big banks only deal with big loans, so small banks are essential as well.
2. Deposit is actually a kind of loan which bank borrows from individuals.
3. The figure showing in your account is just banks' debt.
4. Loans for productive purposes won't lead to inflation, but for nonproductive ones will.
5. Spemding drives economy.
6. Credit could be good or bad,depending on how you use them.
7. Short term debt cycle is mainly under control of central bank's interest.
8. Long term debt cycle is caused by human nature leading to borrowing beyond paying back.

# 181011 Week 5

1. Big bank is easier to fail.
2. Besides withdrawing from banks, the way like Lehman Brothers is another probability for banks to go bankrupt.
3. Money is something having ability to store value.
4. Currency is not "Money".
5. Bitcoin is somehow like investments.
6. Nationalism tends to have a peace world.
7. Fascism claims every citizen has obligation to only support his or her nation.
8. Fascism believes that only one winner exists and the rest are loser ,which is a problem leading to destruction.
9. It's was "Silence" of good people that leads to Extremism or Fascism going wrong,e.g.,WW2.
10. The merger of information tech. with bio tech. may create a algorism that knows me better than I know myself.Then, we may be manipulated by those who control the data.
11. In the journey of life, encountering too many "Potholes" we can't navigate around make good person do something bad.
12. Receiving compassion from the people you least deserve it from.

# 181018 Week 6 

1. The true thing is a combination of lots of knowledges justified with believe.
2. Rwanda is a great example which invested in public health-care and well-fare to urge two hated tribes become one peaceful country.
3. Selection bias tends to mislead individuals.
4. Exercise can help brain strong against brain-disease.
5. Health-care system leads to a much more efficient and low-cost medical system than sick-care system.

# 181025 Week 7

1. Depression is a leading cause of ill health.
2. Seven percent of Americans experiencing depression in a year.
3. Depression doesn't diminish a person's desire to connect with others, but ability.
4. For connecting depressed people, words are not the most important thing to focus on.
5. Ace students are more likely to have bipolar condition.
6. Invite depressed people to contribute to your life in some way.
7. Some people have misconceptioon that depresiion was asign of weakness.
8. "Maybe" most mental disorder devolope because of a chemical inbalancein the brain or genetic predisposition.
9. Black americans have twenty percent greater risk of developing a mental disorder, and they seek mental services at half rate of white ones.
10. The suicide rate of black children has doubled in the past twenty years.

# 181101 Week 8

1. How to heip depressed people?
2. People tends to explain their choices, even though it's not their real choice.
3. Belong,Purpose,Transcendence and Storytelling are four pillars for meaning of happy.

# 181108 Week 9

1. You have rights to deny to show your details to plice officer, if you break no law.
2. What people use to create a debt of houndreds thousand dollars is their signatures.
3. Individuals may accept the option they did't choose by tricks just beacause they forgot what they chose. (In video's exp.)
4. Everone of us is sovereign.
5. "Fear" makes people set up government too rule themselves.
6. Government, the creation of man, can never be above "Man".
7. When your parents register your birth, they actually create a company.
8. "Person" in law society, means corporation.

# 181115 Week 10

1. Differance between "Lawfal" and "Legal".
2. It takes long time to build Love than ruin it.
3. Millenials get failed parenting strategy.
4. Doploma increases while people receive text from others.
5. It's vital to reach balance in spending time for social media.
6. "Patience" maybe has been neglected by Millenials due to fast tech device.
7. Algorithms are opinions embeded in codes.
8. Algorithms don't make thing fair, because they repeat our old practices.

# 181122 Week 11

1. Advertisement can change the way people live, e.g., diamond ring for engagement.
2. When everyone is reporter, then nobody is.
3. The strength of one empire is directly related to it's strength of currency. 

# 181129 Week 12

1. Does ecomomy rise always has to look like continous rising curve?
2. Dynamic balance of the world looks like a donut.
3. Regenerate and distribution design are two ways to save the unbalance.

# 181206 Week 13

1. To save the water shortage problem, use cups to store water to brush teeth, instead of keeping flowing the faucet.
2. To save the ozone layer, reduce the use of platrim stic, because processing plastic bags involves CFCs.
3. To save global warming, trim AC to adequate temperature, not too cold that makes people put on jackets.

# 181213 Week 14

* Lecture

1. Private people are owners of Center Banks.
2. Capital is real rich, but stale salary isn't.
3. Marginal cost is like something you need to cost repeatedly for every production.
4. Essential free energy is like renewable energy, e.g., solar power.
5. 7 Habbits we should own: Proactive, Begin with end in mind, Put first thing first, Think win-win, Understand others first, synergize, Sharpen the saw
6. The perfection means they are willing to do something that seems difficult.
7. We should fight ourselves' filters, to enbrace others.
8. We should create random, to provide ourselves the chance to meet new people.

* Task

  2018-12-13
  
1. Successful today, cause I finish a presentation today, I feel released today.
2. Unproductive today, cause I wake up at 10 A.M., spend two hours on YouTube and three hours on online game.
3. I will limit my time spending on online game less than one hour to be more productive. 

   2018-12-14
   
1. Successful today, cause I finish discussion about tomorrow's presentation with my teammates.
2. Unproductive today, because I didn't do anything without taking class and discussing presentation, maybe it's because I wake up too late.
3. I will get up before 9:00, to have more time to be productive.

   2018-12-15
   
1. Successful today, becauae I learn a lot about aviation in today's class, and I also achieve my goal to wake up in 8:30.
2. Unproductive today, cause I don't want to do anything without relax after taking a 5-hours class.
3. I will wash my scooter tomorrow to ensure doing something to rise my productivity.

   2018-12-16
   
1. Successful today, it's a successfully lazy day. Movie, gaming, washing scooter.
2. Unproductive today, still a lazy day...
3. I will study in library to be more productive.

   2018-12-17
   
1. Successful today, I successfully went to library to study in the morning and took graduated photo with my classmates happily.
2. Unproductive today, still wasting time watching Youtube whole night.
3. I will study in library in the morning and afternoon.

   2018-12-18
   
1. Successful today, I have a fulfilled day, studying and taking class in afternoon, strolling at beach and having big meal at night.
2. Unproductive today, wake up late and only study about 2 hours.
3. I will finish the things that I listed but not finished recently.

   2018-12-19
   
1. Successful today, I finished the listed things and lhave fun in study group, having a fulfilled day.
2. Productive today, I finished some to-do-listed thing, and learned a lot in study group.
3. I will start preparing TOEIC SW tomorrow.

   Five rules
1. Review today  

2. Set a goal for tomorrow

3. Go to library for studying

4. Limit Youtube or gaming time

5. List a To-D0-list.

# 181220 Week 15

1. "Sortition" is a way to democracy.
2. Pieces of litter can be seen as data to do statistics.
3. Bhutan is a country in the stituation of "Carbon negative", and it's institution ensures there is at least 60% of green-land. 

# 181227 Week 16

1. School should teach students being brave to express their opinion and also listening others.
2. Making people feel close to the thing is the best way to convey anything.
3. Web 3.0 is coming.

# 180103 Week 17

1. Sales skill includes "put yourslf in customer's shoe" to realize what they want and what problems ca be solved for them by the product.
2. Show your unique side of you on online dating.
3. Key figure for encountering best partner:37%
4. Heart breaking due to breaking up affects people like a drug.
